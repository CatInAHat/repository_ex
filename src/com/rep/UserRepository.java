package com.rep;

public interface UserRepository {
	
		public void withLogin(String login);
		public void withLoginAndPassword(String login, String password);
		public void setupPermission(User user);
}
