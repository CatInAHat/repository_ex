package com.rep;

public interface Repository<TEntity>{
	
	public void withId(int id);
	public void allOnPage(PageingInfo page);
	public void add(TEntity entity);
	public void delete(TEntity entity);
	public void modify(TEntity entity);
	public void count();
	

}
