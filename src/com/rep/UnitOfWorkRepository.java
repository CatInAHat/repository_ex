package com.rep;

import javax.swing.text.html.parser.Entity;

public interface UnitOfWorkRepository {
	
		public void persistAdd(Entity entity);
		public void persistDelete(Entity entity);
		public void persistUpdate(Entity entity);
}
