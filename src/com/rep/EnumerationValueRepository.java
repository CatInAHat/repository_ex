package com.rep;

public interface EnumerationValueRepository {

		public void withName(String name);
		public void withIntKey(int key, String name);
		public void withStringKey(String key, String name);
}
